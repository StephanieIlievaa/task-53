import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  let product = document.querySelector('.product');
  //console.log(product);
  let priceElement = document.querySelector(".price").textContent;
  //console.log(priceElement);
  product.setAttribute('data-price', priceElement.valueOf());

});
